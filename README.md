# flowbot for Discord
A Discord bot that fetches information about University of Waterloo courses from https://uwflow.com. Written in JavaScript using Node.js v14, discord.js and Puppeteer. In active development.

## Disclaimer
This bot is not made by, endorsed by or affiliated with the makers of UW Flow. However, it is made possible by their website! Visit https://uwflow.com for more detailed information about UW courses and professors.

## Invite flowbot
Invite flowbot to your server using this link: https://discord.com/oauth2/authorize?client_id=851968179404013608&scope=bot&permissions=2147748928

## Setup
Follow these instructions to run the bot locally.
1. Save the repository and navigate to the directory in a terminal.
2. Rename `.env_sample` to `.env` and adjust to your configuration.
3. Run `npm run build` to download dependencies and initialize `stats.json`
4. Run `npm run register` to register slash commands on the client. This may take up to 1 hour for Discord to process.
5. Run `npm start` to start the bot.

## Usage
### !course `[course code]`
Fetches UW course information from https://uwflow.com
![!course CS 115](./readme_files/course.jpg)
### !help
For a full list of commands and usage, type `!help`
![!help](./readme_files/help.jpg)