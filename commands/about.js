module.exports = {
    name: 'about',
    desc: 'More information about the bot.',
    usage: 'about',
    execute: async (m, stats, Discord, client) => {
        m.reply({
            embeds: [
                new Discord.MessageEmbed()
                    .setDescription('This bot is not made by, endorsed by or affiliated with the makers of UW Flow. However, it is made possible by their website! Visit <https://uwflow.com/> for more detailed information about UW courses and professors.')
                    .setColor(client.colour)
                    .setFooter(`Issued ${stats["cmd"]} commands | Served ${stats["unique"].length} users | ${client.guilds.cache.size} servers`),
            ],
            components: [
                new Discord.MessageActionRow()
                    .addComponents(
                        new Discord.MessageButton()
                            .setLabel('Visit UW Flow')
                            .setStyle('LINK')
                            .setURL('https://uwflow.com/'),
                        new Discord.MessageButton()
                            .setLabel('Invite flowbot')
                            .setStyle('LINK')
                            .setURL(client.generateInvite({
                                scopes: ['bot', 'applications.commands'],
                                permissions: [
                                    'VIEW_CHANNEL',
                                    'SEND_MESSAGES',
                                    'USE_EXTERNAL_EMOJIS',
                                    'ADD_REACTIONS',
                                    'USE_APPLICATION_COMMANDS'
                                ]
                            }))
                    )
            ],
            failIfNotExists: false,
            allowedMentions: { repliedUser: false },
        });
    }
}