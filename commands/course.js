module.exports = {
    name: 'course',
    aliases: ['c'],
    desc: 'Get information on a course from UW Flow!',
    usage: 'course CS 115',
    buildSlash: SlashCommandBuilder => SlashCommandBuilder
        .addStringOption(option => option
            .setName('course-code')
            .setDescription('The course code to search for on UW Flow (ex. CS 115)')
            .setRequired(true)
        ),
    execute: async (m, ARGS, browser, UWFLOW, Discord, client) => {
        const MAX_LEN = 15 // maximum course name length
        const courseName = (ARGS.join('').match(/[a-zA-Z0-9]/g) || []).join('').toUpperCase();

        // === CANCEL INVALID INPUT ===
        // returns if empty string, non-alphanumeric or longer than MAX_LEN characters
        if (!courseName || courseName.length > MAX_LEN) {
            return m.reply({
                content: 'You need to specify a valid course code! ex. `/course CS 115`',
                failIfNotExists: false,
                allowedMentions: { repliedUser: false },
            }).catch();
        }

        // send confirmation message
        const message = await m.reply({
            content: `${client.emotes.load} Searching for course \`${courseName}\``,
            failIfNotExists: false,
            allowedMentions: { repliedUser: false },
            fetchReply: true,
        });

        // === NAVIGATE TO UW FLOW COURSE PAGE ===
        const URL = UWFLOW + 'course/' + courseName;
        const page = await browser.newPage();
        let course;
        try {
            await page.goto(URL, { waitUntil: 'networkidle0', timeout: 10000 });

            // === GET COURSE INFORMATION FROM UW FLOW PAGE AS OBJECT LITERAL ===
            // Note: many hard-coded CSS class names, may fail if UW Flow website layout has been changed
            course = await page.evaluate(() => {
                const $ = className => document.getElementsByClassName(className); // get element by class name
                const req = $('sc-pYNsO iaFdeJ'); // requisite info
                const rate = $('sc-pcJja jjDvpo'); // rating info

                const courseObj = {
                    id: $('sc-pktCe eHAbVk')[0]?.textContent, // course code
                    name: $('sc-qPLKk jUTLDm')[0]?.textContent, // full name
                    desc: $('sc-pAyMl dPzwlT')[0]?.textContent, 
                    prereq: req[0]?.nextElementSibling?.textContent,
                    coreq: req[1]?.nextElementSibling?.textContent,
                    antireq: req[2]?.nextElementSibling?.textContent,
                    leadsTo: req[3]?.nextElementSibling,
                    ratings: `${$('sc-psQdR iYtUny')[0]?.textContent} liked | ${rate[0]?.textContent} easy | ${rate[1]?.textContent} useful`
                };

                // handle course corequisites
                if (courseObj.coreq == 'Courses required to be taken concurrently with, or prior to, this course.') {
                    courseObj.coreq = $('sc-pYNsO iaFdeJ')[1]?.nextElementSibling?.nextElementSibling?.textContent;
                }

                // handle course leads to
                if (!courseObj.leadsTo) {
                    return null;
                } else if (courseObj.leadsTo?.textContent == 'No other courses') {
                    courseObj.leadsTo = courseObj.leadsTo.textContent;
                } else {
                    let el = courseObj.leadsTo;
                    courseObj.leadsTo = "";
                    for (el; el?.textContent != null; el = el.nextElementSibling) {
                        // add course code to string
                        courseObj.leadsTo += el.textContent.split(/ +/).slice(0, 2).join(' ') + ', ';
                    }
                    // remove trailing comma
                    courseObj.leadsTo = courseObj.leadsTo.slice(0, -2);
                }
                return courseObj;
            });
        } catch (e) {
            console.error(e);
            message.edit(`${client.emotes.err} Sorry, an error occurred.`);
        } finally {
            await page.close();
        }
        
        // === HANDLE COURSE DOES NOT EXIST ON UW FLOW ===
        if (!course) {
            return message.edit(
                `${client.emotes.err} Couldn't find a course called \`${courseName}\` on UW Flow. Please enter an existing course code or try searching for it using \`/search\`. \nex. \`/course CS 115\` ${m.member}`
            );
        };

        // discord button component link to course page
        const urlButton = new Discord.MessageButton()
            .setLabel('View on UW Flow')
            .setStyle('LINK')
            .setURL(URL);

        // === EDIT MESSAGE WITH COURSE INFORMATION ===
        message.edit({
            content: `${course.id}: ${course.name}`,
            embeds: [
                new Discord.MessageEmbed()
                    .setTitle(`${course.id}: ${course.name}`)
                    .setURL(URL)
                    .setColor(client.colour)
                    .setDescription(course.desc + '\n\u200B')
                    .addFields(
                        { name: 'Prerequisites', value: course.prereq, inline: true },
                        { name: 'Corequisites', value: course.coreq, inline: true },
                        { name: 'Antirequisites', value: course.antireq, inline: true },
                        { name: 'Leads to', value: course.leadsTo, inline: true }
                    )
                    .setFooter(`${course.ratings} | Requested by ${m.member.user.tag}`),
            ],
            components: [
                new Discord.MessageActionRow()
                    .addComponents(urlButton)
            ],
            allowedMentions: { repliedUser: false }
        });
    }
}