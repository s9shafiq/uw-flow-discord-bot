module.exports = {
    name: 'eval',
    desc: '',
    usage: 'eval [expr]',
    owner: true,
    execute: async (m, client, browser, Discord, OWNER_ID) => {
        if (m.author.id != OWNER_ID) return;
        let evalStr = m.content.slice('eval '.length);
        // handle implicit return on one line eval
        evalStr = (evalStr.includes('\n')) ? `{${evalStr}}` : evalStr;
        try {
            let evalRes = await eval(`(async () => ${evalStr})()`);
            if (!evalRes) return m.react(client.emotes.err);
            m.channel.send(evalRes.toString());
        } catch (e) {
            m.channel.send('```' + e + '```');
        }
    }
}