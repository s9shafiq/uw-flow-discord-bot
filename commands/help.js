module.exports = {
    name: 'help',
    desc: 'More information about flowbot commands.',
    usage: 'help',
    buildSlash: SlashCommandBuilder => SlashCommandBuilder
    .addStringOption(option => option
        .setName('command')
        .setDescription('A specific command you want help with')
    ),
    execute: async (m, name, PREFIX, Discord, client) => {
        const commands = client.commands.filter(command => !command.owner);

        if (commands.has(name)) {
            const info = commands.get(name);
            return m.reply({
                embeds: [
                    new Discord.MessageEmbed()
                        .setTitle(`${PREFIX}${name}`)
                        .setDescription(`${info.desc} \nUsage: \`${PREFIX}${info.usage}\``)
                        .setColor(client.colour)
                ],
                failIfNotExists: false,
                allowedMentions: { repliedUser: false },
            });
        }

        let commandList = "";
        client.commands.filter(command => !command.owner).forEach(command => commandList += `${PREFIX}${command.name}, `)
        commandList = commandList.slice(0, -2); // remove trailing comma

        m.reply({
            embeds: [
                new Discord.MessageEmbed()
                    .setDescription(`For more information on a command, type \`${PREFIX}help [command]\`. \n\`${commandList}\``)
                    .setColor(client.colour),
            ],
            failIfNotExists: false,
            allowedMentions: { repliedUser: false },
        });
    }
}