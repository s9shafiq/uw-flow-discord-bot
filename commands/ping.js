module.exports = {
    name: 'ping',
    desc: 'Check if the bot is up!',
    usage: 'ping',
    execute: async (m) => {
        m.reply({
            content: 'Pong!',
            failIfNotExists: false,
            allowedMentions: { repliedUser: false },
        });
    }
}