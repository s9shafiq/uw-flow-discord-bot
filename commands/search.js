module.exports = {
    name: 'search',
    aliases: ['s'],
    desc: 'Search for a course on UW Flow!',
    usage: 'search Calculus 1',
    buildSlash: SlashCommandBuilder => SlashCommandBuilder
        .addStringOption(option => option
            .setName('query')
            .setDescription('The search terms to find a course on UW Flow (e.g., Calculus 1)')
            .setRequired(true)
        ),
    execute: async (m, ARGS, browser, UWFLOW, Discord, client) => {
        const MAX_LEN = 99 // maximum query length
        const query = (ARGS.join(' ').match(/[a-zA-Z0-9 ]/g) || []).join('');

        // === CANCEL INVALID INPUT ===
        // returns if empty string, non-alphanumeric or longer than MAX_LEN characters
        if (!query || query.length > MAX_LEN) {
            return m.reply({
                content: 'You need to specify valid search terms! ex. `/search Calculus 1`',
                failIfNotExists: false,
                allowedMentions: { repliedUser: false },
            }).catch();
        }

        // send confirmation message
        const message = await m.reply({
            content: `${client.emotes.load} Searching for \`${query}\` on UW Flow`,
            failIfNotExists: false,
            allowedMentions: { repliedUser: false },
            fetchReply: true,
        });

        // === NAVIGATE TO UW FLOW COURSE PAGE ===
        const URL = UWFLOW + 'explore?q=' + query.replace(/ /g, '+');
        const page = await browser.newPage();
        let courses;
        try {
            await page.goto(URL, { waitUntil: 'networkidle0', timeout: 10000 });

            // === GET SEARCH INFORMATION FROM UW FLOW PAGE AS ARRAY ===
            // Note: many hard-coded CSS class names, may fail if UW Flow website layout has been changed
            courses = await page.evaluate(() => {
                // get result elements
                const results = document.getElementsByClassName('sc-pAncQ gGqvRj')[0]?.children;

                if (!results.length) {
                    return null;
                }

                const resultsArr = [];
                for (let i = 0; i < results.length; i++) {
                    const course = {
                        code: results[i].firstElementChild?.firstElementChild?.textContent,
                        name: results[i].firstElementChild?.nextElementSibling?.textContent
                    }
                    resultsArr[i] = `**${course.code}:** ${course.name}`;
                }

                return resultsArr;
            });
        } catch (e) {
            console.error(e);
            message.edit(`${client.emotes.err} Sorry, an error occurred.`);
        } finally {
            await page.close();
        }

        // === HANDLE NO RESULTS ON UW FLOW ===
        if (!courses) {
            return message.edit(
                `${client.emotes.err} No results for \`${query}\` on UW Flow. ${m.member}`
            );
        };

        // discord button component link to course page
        const urlButton = new Discord.MessageButton()
            .setLabel('View on UW Flow')
            .setStyle('LINK')
            .setURL(URL);

        // === EDIT MESSAGE WITH SEARCH RESULTS ===
        const RESULTS_PER_PAGE = 10;
        let currentPage = 0;

        message.edit({
            content: `Found ${courses.length} courses`,
            embeds: [
                new Discord.MessageEmbed()
                    .setTitle(`🔍 \`${query}\` on UW Flow`)
                    .setURL(URL)
                    .setColor(client.colour)
                    .setDescription(courses.slice(currentPage * RESULTS_PER_PAGE, (currentPage + 1) * RESULTS_PER_PAGE).join('\n'))
                    .setFooter(`${currentPage + 1}/${Math.ceil(courses.length / RESULTS_PER_PAGE)} pages | Requested by ${m.member.user.tag}`),
            ],
            components: [
                new Discord.MessageActionRow()
                    .addComponents(
                        new Discord.MessageButton()
                            .setCustomId('prev')
                            .setStyle('PRIMARY')
                            .setEmoji('◀'),
                        new Discord.MessageButton()
                            .setCustomId('next')
                            .setStyle('PRIMARY')
                            .setEmoji('▶'),
                        new Discord.MessageButton()
                            .setCustomId('exit')
                            .setStyle('DANGER')
                            .setEmoji('✖'),
                        urlButton,
                    )
            ],
            allowedMentions: { repliedUser: false }
        });

        // === HANDLE NAV BUTTONS ===
        const collector = message.createMessageComponentCollector({ time: 60000 });
        const handleNav = i => i.update({
            embeds: [
                new Discord.MessageEmbed()
                    .setTitle(`🔍 \`${query}\` on UW Flow`)
                    .setURL(URL)
                    .setColor(client.colour)
                    .setDescription(courses.slice(currentPage * RESULTS_PER_PAGE, (currentPage + 1) * RESULTS_PER_PAGE).join('\n'))
                    .setFooter(`${currentPage + 1}/${Math.ceil(courses.length / RESULTS_PER_PAGE)} pages | Requested by ${m.member.user.tag}`),
            ],
        });

        collector.on('collect', i => {
            if (i.user.id != m.member.id) return i.reply({ content: `Only ${m.member} can interact with this message!`, ephemeral: true });
            if (i.customId == 'prev') {
                if (!currentPage) return i.reply({ content: 'You\'re already on the first page!', ephemeral: true });

                currentPage--;
                handleNav(i);
            } else if (i.customId == 'next') {
                if (currentPage == Math.floor(courses.length / RESULTS_PER_PAGE)) return i.reply({ content: 'You\'re already on the last page!', ephemeral: true });

                currentPage++;
                handleNav(i);
            } else if (i.customId == 'exit') {
                i.update({
                    components: [
                        new Discord.MessageActionRow()
                            .addComponents(urlButton)
                    ],
                })
                collector.stop('manual');
            }
        })

        // === HANDLE ENDING NAVIGATION ===
        collector.on('end', (_, reason) => {
            if (reason != 'manual') message.edit({
                components: [
                    new Discord.MessageActionRow()
                        .addComponents(urlButton)
                ],
            });
        });
    }
}