module.exports = {
    name: 'shutdown',
    desc: '',
    usage: 'shutdown',
    owner: true,
    execute: async (m, client, browser, OWNER_ID) => {
        if (m.author.id != OWNER_ID) return;

        await m.channel.send('Shutting down');
        await browser.close();
        client.destroy();
        process.exit();
    }
}