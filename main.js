require('dotenv').config();
const Discord = require('discord.js');
const puppeteer = require('puppeteer');
const fs = require('fs');

const client = new Discord.Client({ intents: ['GUILDS', 'GUILD_MESSAGES'] });

// === MANAGE STATS JSON ===
const stats = JSON.parse(fs.readFileSync('./stats.json'));
const STAT_INTERVAL = process.env['STAT_INTERVAL'] || 300000; // 5 min default
setInterval(() => fs.writeFile('./stats.json', JSON.stringify(stats, null, 1), () => { }), STAT_INTERVAL); // update stats.json periodically

// === ENVIRONMENT VARIABLES ===
const COOLDOWN_TIME = process.env['COOLDOWN_TIME'] || 1500; // cooldown time in ms, 1.5s default
const PREFIX = process.env['PREFIX'] || '!';
const UWFLOW = process.env['UWFLOW_URL']; // base uw flow url
let browser;

// === INITIALIZE CLIENT PROPERTIES ===
// essential client properties
client.cooldown = new Discord.Collection();
client.commands = new Discord.Collection();
let COMMAND_ALIASES = [];
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
for (const fileName of commandFiles) {
    const command = require(`./commands/${fileName}`);
    client.commands.set(command.name, command);
    if (command.aliases) COMMAND_ALIASES = COMMAND_ALIASES.concat(command.aliases);
}
// stylistic client properties
client.colour = '#0052CC';
client.emotes = {
    load: process.env['LOAD_EMOTE'] || '⌛',
    err: process.env['ERR_EMOTE'] || '❌',
}


// === CLIENT IS ONLINE ===
client.on('ready', async () => {
    console.log('=== BOT IS ONLINE ===');
    client.user.setActivity('/course')
    browser = await puppeteer.launch({
        args: ['--no-sandbox', '--disable-setuid-sandbox'],
    }).catch(e => {
        console.error(e);
        client.destroy();
        process.exit();
    });
})

// === COMMAND HANDLER ===
const handleCommand = (m, COMMAND, ARGS = []) => {
    if (!m.guild) return;
    switch (COMMAND) {
        case 'ping':
            client.commands.get('ping').execute(m);
            break;
        case 'help':
            client.commands.get('help').execute(m, ARGS[0], '/', Discord, client);
            break;
        case 'about':
            client.commands.get('about').execute(m, stats, Discord, client);
            break;
        case 'c':
        case 'course':
            client.commands.get('course').execute(m, ARGS, browser, UWFLOW, Discord, client);
            break;
        case 's':
        case 'search':
            client.commands.get('search').execute(m, ARGS, browser, UWFLOW, Discord, client);
            break;
        case 'shutdown':
            client.commands.get('shutdown').execute(m, client, browser, process.env['OWNER_ID']);
            break;
        case 'eval':
            client.commands.get('eval').execute(m, client, browser, Discord, process.env['OWNER_ID']);
            break;
        default:
            return false;
    }

    // === STATS HANDLER ===
    stats["cmd"]++;
    if (!stats["unique"].includes(m.member.id)) stats["unique"].push(m.member.id);
}

// === CLIENT RECEIVES MESSAGE IN SERVER === 
client.on('messageCreate', async m => {
    // === IGNORE INELIGIBLE MESSAGES ===
    if (!m.content.startsWith(PREFIX) || m.author.bot || !m.guild?.me.permissions.has('SEND_MESSAGES')) return;

    const ARGS = m.content.toLowerCase().slice(PREFIX.length).split(/ +/);
    const COMMAND = ARGS.shift();

    if (!(client.commands.has(COMMAND) || COMMAND_ALIASES.includes(COMMAND))) return;

    // === HANDLE MESSAGE COOLDOWN ===
    if (client.cooldown.has(m.author.id)) { // issue warning
        const message = await m.reply(`Sorry! There's a **${COOLDOWN_TIME / 1000} second cooldown** between commands.`);
        setTimeout(() => message.delete(), 3000) // delete cooldown warning after three seconds
        return;
    } else { // add user to cooldown
        client.cooldown.set(m.author.id, null);
        setTimeout(() => client.cooldown.delete(m.author.id), COOLDOWN_TIME);
    }

    // === COMMAND HANDLER ===
    handleCommand(m, COMMAND, ARGS);
});

// === CLIENT RECEIVES SLASH COMMAND IN SERVER === 
client.on('interactionCreate', interaction => {
    // === IGNORE INELIGIBLE INTERACTIONS ===
    if (!interaction.isCommand()) return;
    // === COMMAND HANDLER ===
    handleCommand(interaction, interaction.commandName, interaction.options.data.map(a => a.value));
})

client.login(process.env['TOKEN']);